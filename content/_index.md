---
title: "Welcome to MCnet!"
page: true
menu:
  main:
    name: "Home"
    weight: 1
---

MCnet is an international community of high-energy physics theorists,
experimentalists, and software developers behind the leading techniques and
tools for simulating fully exclusive particle-collision events.

MCnet members are responsible for the leading shower+hadronisation
event-generator code families Herwig, Pythia, MadGraph/aMC@NLO, and Sherpa, key
to particle-physics experiments including those at the CERN Large Hadron
Collider.

Previously the MCnetITN3 EU Innovative Training Network, MCnet is now an
umbrella community for all working in methods, codes, and applications of
general-purpose MC event generators. We coordinate interoperability of tools,
support the experiment and phenomenology physics communities, and provide
training and community events including workshops, meetings and summer schools.
