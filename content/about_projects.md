---
title: "About"
lead: "Projects"
menu:
  side_about:
    name: "Projects"
    weight: 1
sidebar: right
widgets: ["sidemenu_about", "project_home_pages"]
---

The work of MCnet is divided into several projects.

**Pythia**, **Herwig** and **Sherpa** are all projects to develop the physics
  models and implementations of general-purpose event generators and support
  their use by the LHC experiments and elsewhere.

**MadGraph** and **Ariadne** are projects to develop implementations and
  alternative approaches to two of the most important steps of event generation:
  automated hard-process generation and parton-showering evolution, particularly
  in the high-energy limit.

**CEDAR** is a more general project aimed to provide general-purpose facilities
  to do high energy physics analysis and phenomenology. Among its many
  sub-projects, **Rivet** and **Professor** are particularly important for
  MCnet, providing generator-independent frameworks for comparison of event
  generator predictions with corrected data and for tuning event generator
  parameters. Currently active projects that build on Rivet's event-analysis
  database include **Contur**, a toolkit for re-interpretation of published
  collider data via MC modelling of new-physics signatures.

***

Pythia and Vincia
-----------------

![](/img/projects/pythia.png)

Pythia has its roots in the Jetset program, begun more than 30 years ago. It has been used by generations of physicists, notably at LEP and the Tevatron, and for LHC preparations, but also in areas such as astroparticle physics. Traditionally Pythia has put strong emphasis on soft physics, such as hadronization, minimum-bias physics and the soft underlying event. At the same time it contains a wide selection of Standard Model (SM) and Beyond-Standard Model (BSM) processes, and modern dipole-style transverse- momentum-ordered parton showers.

The Pythia project includes Vincia as a subproject. Vincia is plugin to Pythia 8 for dipole-antenna showers. Tree-level matrix elements (e.g., from MadGraph) can be incorporated in the shower evolution, using the GKS matching scheme. Vincia also has extensive possibilities for uncertainty estimates, via systematic variations of shower parameters. The development focus in the near future is on extensions of the formalism to hadron collisions, the inclusion of NLO matrix elements in the shower evolution, and further automation of the uncertainty variations.

***

Herwig
------

![](/img/projects/herwig.png)

The Herwig event generator has been used in the planning and data analysis of high energy physics experiments for the past twenty years. Following a major rewrite, the current version simulates lepton and hadron collisions including: careful modeling of parton showers, including coherence effects; a detailed model of the underlying event; several processes accurate at next-to-leading order; simulation of a range of BSM models with spin correlations; a detailed model of hadron and tau decays.

The next major release will include further next-to-leading order processes and the matching of leading-order processes to higher multiplicity matrix elements in an improvement to the original approach of Catani, Krauss, Kuhn and Webber (CKKW). In the longer term we will continue our emphasis on state-of-the-art simulation of perturbative physics.

***

Sherpa
------

![](/img/projects/sherpa.png)

Sherpa is a fully-fledged event generator comprising solutions to all aspects of event simulation. Its hallmark feature is the automated merging of leading-order matrix elements for multijet production with parton showers, provided by two independent matrix element generators for large final state multiplicities in various models, and interfaced to an automated Feynman rule generator. It also provides an automated method to treat the infrared divergent structures emerging in NLO QCD corrections, which is being used for state-of-the-art NLO calculations. First steps towards a truly automated version of the matching of parton showers with NLO matrix elements have met success - Sherpa by now includes all methods necessary to perform such a matching within the Powheg formalism, tested in a large range of processes. At the same time various theoretical problems with both the Powheg and the MC@NLO method have been found, which led the authors to concentrate on the further development of MC@NLO methods alone.

***

MadGraph
--------

![](/img/projects/madgraph.jpg)

MadGraph provides a world-wide on-line service for the generation of events
relevant for studies at present and future high-energy colliders. The
capabilities of the current version 5 include: event generation via the web for
any Standard Model process as well as for new physics models via FeynRules;
fully-staged simulation chain, from model building to detector simulation;
multi-jet sample production and merging; interfaces with the software frameworks
of the main experiments at the LHC and the possibility of event generation over
the computing GRID. Version 5 now distributes publicly the automatic computation
of loop amplitudes (MadLoop), the combination with the real corrections (MadFKS)
and the automatic interface via the MC@NLO method (aMC@NLO) to Herwig and Pythia
for parton showering and hadronization.

***

Plugin
------

The general-purpose event generators, Pythia, Herwig and Sherpa, work well for
most processes at the LHC, but there is still room for improvement. The
development of models is often done outside the three main programs, although
the resulting code typically is interfaced to plug in to one or more of them. A
special class of such programs involves parton shower models where the standard
collinear evolution is amended by so-called small-x resummation. Two such
programs are explicitly included in this project:

- Ariadne/DIPSY is based on dipole formulation of parton showers where the
  initial-state shower is based on evolutionin impact-parameter space. This
  program is also able to model collisions involving heavy-ion collision. Such
  collisions are well integrated parts on the LHC program, but none of the three
  main programs have the capability of fully simulating them.
- HEJ is based on a high-energy version of collinear factorization, where
  small-x resummation is added in the limit of multi-Regge kinematics. The
  program needs to be interfaced to final state showers to get the full modeling
  of the resulting jets. So far there exists a simplified interface to Ariadne,
  and another more sophisticated interface is being developed. Work is also
  ongoing on merging NLO partonic samples within the HEJ formalism.

Besides developing these two programs, the Plugin project will act as an
interface to theorists in the wider community working on special models for
parts of the event generating chain. The project is led by Leif Lonnblad in
Lund, with Jeppe Andersen and Jennifer Smillie of the Durham team.

***

CEDAR
-----

![](/img/projects/cedar.png)

Several activities are collected together as the CEDAR project, with the general
theme of providing tools to interlink the other projects, for validation and
tuning of the generators, and linking with the users of MC generators. While led
from UCL and Glasgow, the project also has key people in CERN, Durham, and
Fermilab.

The CEDAR teams are responsible for the Rivet (Robust Independent Validation of
Experiment & Theory) package, which provides generator-independent analysis
tools that enable MC-model comparisons to the measurements in the HepData
database. HepData functions as the central archive of measurements from previous
and current colliders, including the LHC, and is used by all generator projects
for tuning and validation of models. A major goal is to facilitate
implementation of key LHC data analyses in Rivet as soon as they become
available, and to provide all the functionality needed for analysis logic
preservation.

The output from Rivet can be coupled with a mechanism for generator tuning. The
Professor package provides an automated tool to do this, and is central to the
MC tunes constructed and used by the LHC experiments. Rivet is also the central
component of the Contur project, which uses measurement analyses from collider
experiments to place statistical limits on the viability of new-physics (or
Beyond Standard Model) models, complementing and often pre-empting dedicated
direct-search analyses.

CEDAR also developed and supports community tools, e.g. the software-development
platform HepForge; the LHAPDF library of parton densities; the HepMC event
record standard and code library; the Les Houches accords for data exchange
between generators; and the LHC Computing Grid "Generator Services"
project. CEDAR includes members of the LHC and other experiments, to provide
support, liaison and training.
