---
title: "About"
lead: "Teams > Vienna members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Core team
---------

***

##### Simon Plätzer (Team Leader)

![](/img/vienna/vienna_member_platzer.png) 

Simon is a University Assistant at Vienna