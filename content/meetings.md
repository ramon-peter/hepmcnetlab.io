---
title: "Meetings"
menu:
  main:
    name: "Meetings"
    weight: 6
sidebar: "right"
widgets: ["meeting_links"]
---

There are usually two MCnet network/community meetings per year, including one
with an open session at CERN. See the [MCnet Indico category](https://indico.cern.ch/category/1725/).

***

Upcoming meetings
-----------------

TBA


***

Previous meetings
-----------------

The first MCnet network meeting was held at CERN January 8-12 in 2007.

The second meeting was held in Durham in connection with the Monte Carlo school, the 16th and 17th of April.

The third network meeting was held in Lund January 8-10 in 2008.

The fourth network meeting was held in Debrecen the 11th-12th of August 2008 during the CTEQ-MCnet summer school.

The fifth network meeting was held in Durham January 14th-16th 2009.

In addition, together with HEPtools and Artemis, MCnet held a training event, [MC4LHC](https://indico.cern.ch/event/49675/), at CERN May 4th-8th, aimed primarily at an internal audience of the networks' students and young postdocs.

The sixth network meeting was held together with the network's mid-term review and annual school, in Lund June 29th-30th 2009.

The seventh network meeting was held at CERN the 12th-14th of January 2010.

The eighth network meeting was held in Cambridge the 22nd-24th of September 2010, in conjunction with QCD in the LHC Era, a special meeting in honour of Bryan Webber, on his formal retirement.

The ninth network meeting was held in Manchester the 28th-29th of October 2013.

The tenth network meeting was held at CERN March 31st-April 2nd 2014.

The eleventh network meeting was held at Karlsruhe, October 7th-10th 2014, incorporating the network’s Mid-Term Review Meeting.

The network held an Employability Event focussed on CV-writing and interviews, aimed primarily at its PhD students and postdocs, at CERN May 28th and 29th 2015.

The twelfth network meeting was held at CERN, September 23rd-25th 2015.

The thirteenth network meeting was held at Göttingen, April 4th-6th 2016.

The network held a School of Scientific Computing, also in Göttingen, May 16th-20th 2016.

Grant-writing Day, CERN, Nov. 22nd 2016

The 14th network meeting was held at CERN, November 23rd-25th 2016.

15th Network meetingCERN, April, 5th-7th 2017

16th Network meetingKarlsruhe, 27th-29th September 2017

17th Network meeting CERN, 10th -12th April 2018

The network held the 2nd School of Scientific Computing Göttingen, 3rd-7th September 2018.

The 18th network meeting and MCnetITN3 Mid-Term Review at Louvain-la-Neuve, Belgium January 22nd - 25th 2019.

The 19th network meeting was held 4–6 Sept 2019 at CERN.

The 20th network meeting was held 25–27 Mar 2020 in Göttingen/online.

The MCnet Machine Learning School was held virtually from 22–26 Jun 2020 by Lund University.

The 21st network meeting was held virtually from 9–11 Sept 2020 by Durham University.

The 22nd network meeting was held virtually from 19–21 Apr 2021 by CERN and University of Manchester.

After the Covid pandemic, the 23nd and final MCnetITN meeting was held in Manchester, 6-8 Dec 2021.

The first meeting of the MCnet Community was held from 21–23 Sept 2022 at Universität Graz.


<!--

***

Related meetings
----------------

Here we list Monte Carlo related workshops involving MCnet members:

- Members of MCnet, and several other EU RTNs contributed to the organization of the [Corfu Summer Institute on Elementary Particle Physics and Gravity](http://www.physics.ntua.gr/corfu2010/), August 29th - September 5th 2010, as well as to its [2009 edition](http://www.physics.ntua.gr/corfu2009/).
- The ongoing [HERA-LHC workshop](https://www.desy.de/~heralhc/), which included an [MCnet open presentation](example.com).
- Closely related is the series of [PDF4LHC](https://indico.cern.ch/category/281/) workshops.
- Several MCnet members will participate in [this workshop](example.com) on multiparton interactions.
- There will be a workshop on ["New ideas in hadronization: Intersections between QCD, AdS/CFT and the QGP"](https://www.ippp.dur.ac.uk/Workshops/09/Intersections/) in April 2009 at the IPPP, Durham.
- The Institute for the Physics and Mathematics of the Universe (IPMU) at the Kashiwa campus of the University of Tokyo will host a focus week on "Determination of Masses and Spins of New Particles at the LHC" on 16-20 March 2009, co-organized by Bryan Webber. More details will be available from [here](https://www.ipmu.jp/) shortly.
- The [6th Les Houches Workshop on Physics at TeV Colliders](example.com) will be held between June 8th and June 26th 2009, including five MCnet convenors of the "Tools and MC" session!

and non-MCnet schools involving MCnet members:

- The Helmholtz Alliance "Physics at the Terascale"'s [Monte Carlo School](example.com), DESY, April 21st-24th 2008.
- [YETI '09](example.com) "From the Tevatron to the LHC and beyond", IPPP, Durham, January 12th-14th 2009
- The Helmholtz Alliance "Physics at the Terascale" plans to hold another Monte Carlo School at DESY, April 20th-24th 2009.

-->
