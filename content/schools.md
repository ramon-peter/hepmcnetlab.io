---
title: "Monte Carlo Schools"
menu:
  main:
    name: "Schools"
    weight: 3
sidebar: right
widgets: ["school_home_pages"]
---

Throughout the course of the network we will run one school per year aimed at
advanced PhD students and young postdocs. These schools follow the tradition and
format we established through national schools in the UK in 2005 and Germany in
2006, developed further through the four years of the MCnet RTN, and the two
schools we organised without EU funding, in Kyoto, Japan, and at CERN.

The schools are organized by individual teams in, or close to, their
universities. They are typically held in early summer, with precise dates set
with input from the user community concerning experimental collaboration
meetings and other schools and workshops.

An important part of the MCnet school format are our tutorial sessions, in which
students usually work within a virtual machine, through a set of examples and
problems on our main event-generator projects, as well other related challenges,
like writing your own simple parton-shower generator. Examples of the virtual
machine/docker images and tutorial problems can be found on
[GitLab](https://gitlab.com/hepcedar/mcnet-schools).

***

Zakopane 2022
-------------

The 15th MCnet school and 62nd Cracow School of Theoretical Physics will be held in Zakopane, near Cracow, in Poland.

The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures, practical sessions, and discussions with event-generator authors. The school is aimed at advanced doctoral students and early-career postdocs.

Our core sessions comprise a series of introductory lectures on the physics of event generators, further lectures on a wider range of associated topics, a series of hands-on tutorials using all of the MCnet event generators for LHC physics, and evening discussion sessions with Monte Carlo authors.


***

Karlsruhe/Dresden 2021
----------------------

The 14th MCnet school and 27th CTEQ summer school will be held at the Hotel Amselgrundschlösschen, Rathen, Germany (about half an hour from Dresden), with arrival on Sunday September 5th and departure on Wednesday September 15th 2021.

The school provides training on the physics at high energy colliders, particularly the LHC. We will have series of lectures on basic and more advanced topics, practical sessions, and evening discussions with the lecturers. The school is aimed at advanced doctoral students and young postdocs.

Our core sessions comprise a series of introductory lectures on the physics of Monte Carlo event generators and Quantum Chromodynamics , further lectures on a wider range of associated topics, a series of hands-on tutorials using all of the MCnet event generators for LHC physics, and evening discussion sessions with all lecturers.

***

China 2021
----------

The MCnet Beijing School 2021 on Monte Carlo Event Generators will be hosted by the Huairou campus of UCAS, with arrival on Sunday (27/June/2021) and departure on Saturday (03/July/2021).

The school will be jointly supported by MCnet organization, the Center for Future High Energy Physics (CFHEP), the Institute of High Energy Physics (IHEP) of Chinese Academy of Sciences, Peking University, and UCAS.

The MCnet School 2021 Beijing will provide a five-day course of training in high energy physics and computing techniques used in modern Monte Carlo event generators via a series of lectures, tutorial sessions, and discussions with event-generator authors and developers. The school is aimed at advanced doctoral students, early-career postdocs, and young faculty majored in particle physics.

Our core sessions comprise a series of introductory lectures on the physics of event generators, further lectures on a wider range of associated topics, a series of hands-on tutorials using all of the MCnet event generators for LHC physics, and evening discussion sessions with Monte Carlo authors and developers.

***

UCL 2019
--------

The 2019 MCnet Summer School on Monte Carlo Event Generators for the Large Hadron Collider will be held at Cumberland Lodge, located in Windsor Great Park (about an hour from Central London), with arrival on Monday (morning) July 29th and departure on Friday (afternoon) August 2nd 2019.

The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures, practical sessions, and discussions with event-generator authors. The school is aimed at advanced doctoral students and young postdocs.

Our core sessions comprise a series of introductory lectures on the physics of event generators, further lectures on a wider range of associated topics, a series of hands-on tutorials using all of the MCnet event generators for LHC physics, and evening discussion sessions with Monte Carlo authors.

More details will appear here in due course.

***

Vietnam 2019
------------

The 2019 MCnet Vietnam School on Monte Carlo Event Generators for the Large Hadron Collider will be held at ICISE, Quy Nhon, Vietnam, with arrival on Sunday September 15th and departure on Friday (afternoon) September 20th 2019.

The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures, practical sessions, and discussions with event-generator authors. The school is aimed at advanced doctoral students and young postdocs.

Our core sessions comprise a series of introductory lectures on the physics of event generators, further lectures on a wider range of associated topics, a series of hands-on tutorials using all of the MCnet event generators for LHC physics, and evening discussion sessions with Monte Carlo authors.

Monash 2018
-----------

The 2018 MCnet Summer School on Monte Carlo Event Generators for the Large Hadron Collider will be held at [Monash University's Prato campus](https://www.monash.it/), located in the Vai Palace in the historical centre of Prato, Italy (about 20 mins from Florence), with arrival on Sunday July 22nd and departure on Friday (afternoon) July 27th 2018. Convenient airports include Florence, Pisa, and Bologna.
The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures, practical sessions, and discussions with event-generator authors. The school is aimed at advanced doctoral students and young postdocs.

Our core sessions comprise a series of introductory lectures on the physics of event generators, further lectures on a wider range of associated topics, a series of hands-on tutorials using all of the MCnet event generators for LHC physics, and evening discussion sessions with Monte Carlo authors. The full list of lectures is:

- **Introduction to event generators**
(Stefan Gieseke (KIT))
- **Measurements and Monte Carlo**
(TBA)
- **From Lagrangians to Events**
(TBA)
- **Matching and Merging**
(Keith Hamilton (UCL))
- **Showers and Resummation**
(Mike Seymour (U of Manchester))
- **Heavy-Ion Modelling**
(Leif Lönblad (Lund U))
- **Industrial Applications**
(TBA)

The school does not charge a participation fee. However, the participant's home institute must still agree to cover travel, local accommodation, and meals expenses (apart from lunches which are provided by the school). A small number of bursaries are available for students in need. Places are limited.

Applications must be received by **1st of April 2018**. Bursary decisions will be made after that date - students that have an urgent need for earlier information on acceptance should [contact the organisers](mailto:school@montecarlonet.org).

Further details including application procedures can be found at the school homepage.

***

Lund 2017
---------

The Monte Carlo School in 2017 will be organised by the Lund team. It will take place in the city of Lund (Sweden), with arrival on Sunday 2 July and departure Friday afternoon 7 July.
The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures and practical sessions. The school is aimed at advanced doctoral students and young postdocs.

Our core sessions comprise a series of introductory lectures on the physics of event generators, further lectures on a wide range of topics, a series of hands-on tutorials using all of the MCnet event generators for LHC physics, and evening discussion sessions with Monte Carlo authors. The full list of lectures is:

- **Introduction to event generators**
(Peter Skands (Monash, Melbourne))
- **Matching and merging**
(Stefan Höche (SLAC, Stanford))
- **Heavy-ion and cosmic-ray generators**
(Klaus Werner (SUBATECH, Nantes))
- **Simulation of Beyond-the-Standard-Model physics**
(Valentin Hirschi (ETH, Zürich))
- **Effective field theories**
(Eleni Vryonidou (NIKHEK, Amsterdam))
- **Simulation of neutrino physics**
(Costas Andreopoulos (Rutherford & Liverpool))
- **Tuning of generators**
(Holger Schulz (Durham))
- **Machine learning and applications (industrial application)**
(Michel Herquet (B12 Consulting, Louvain-la-Neuve))
- **Outreach**
(Christina Isaxon (Lund))

Most local expenses for students are funded by the EU through the MCnetITN3 Initial Training Network: there is no registration fee, and student housing and some meals will be provided free of charge. A few travel bursaries are available for participants from Convergence Regions of the EU and others in financial need, whose participation would otherwise be prevented.

Applications must be received by **1st of May 2015**. Bursary decisions will be made after that date - students that have an urgent need for earlier information on acceptance should [contact the organisers](mailto:school@montecarlonet.org).

The school with be held at the Department of Astronomy and Theoretical Physics. With students having left for the summer break, nearby downtown Lund displays a relaxed small-city atmosphere.

Further details including application procedures can be found at the school homepage.

***

DESY 2016
---------

The school organised in assocation with CTEQ at DESY.

It will take place at DESY, with arrival on Wednesday 6th July and departure on Saturday 16th July (morning) 2016.

***

Louvain 2015
------------

The Monte Carlo School in 2015 will be organised by the Louvain team. It will take place at the [Sol Cress Domain](https://solcress.be/) in Spa (Belgium), with arrival on Sunday August 30th and departure on Friday (afternoon) September 4th 2015.

The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures and practical sessions. The school is aimed at advanced doctoral students and young postdocs.

Our core sessions comprise: a series of introductory lectures on the physics of event generators by Leif Lönnblad (THEP, Lund) a series of hands-on tutorials using all of the MCnet event generators for LHC physics; and evening discussion sessions with Monte Carlo authors. The full list of lectures is:

- **Introduction to event generators**
(Leif Lönnblad (Lund))
- **NLO computations in QCD**
(Rikkert Frederix (UCL, London, tbc))
- **Model independent measurements**
(Jonathan Butterworth (UCL, London))
- **From BSM simulations to recasting**
(Benjamin Fuks (LPTHE, Strasbourg))
- **Dark Matter simulations**
(Uli Haisch (Univ., Oxford))
- **Analytic resummation and the link with MCs**
(Thomas Becher (ITP, Bern))
- **The magic of colour**
(Malin Sjödahl (THEP, Lund))
- **Industrial application: Proton beams for medical applications**
(Frederic Stichelbaut (IBA, Louvain))

A registration fee of 100 euro covers the participant's portion of accommodation and meals for the duration of the school - all other local expenses are funded by the EU through the MCnetITN Initial Training Network. Fee waivers and travel bursaries are available for participants from Convergence Regions of the EU and others in financial need, whose participation would be otherwise be prevented. Applications must be received by 1st of June 2015. Bursary decisions will be made after that date - students that have an urgent need for earlier information on acceptance should contact the organisers at [contact the organisers](mailto:school@montecarlonet.org).

The school location will be in the Ardenne region in Belgium, in the charming town of Spa walking distance from the famous Thermes and near to the F1 circuit of Francorchamps.

Further details including application procedures can be found at the school homepage.

***

Manchester 2014
---------------

The Monte Carlo School in 2014 will be organised by the Manchester team. It will take place at the Ambleside Campus of the University of Cumbria, with arrival on Sunday August 24th and departure on Saturday August 30th 2014.
The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures and practical sessions. The school is aimed at advanced doctoral students and young postdocs.

Our core sessions comprise: a series of introductory lectures on the **physics of event generators** by Sakurai Prize-winning Monte Carlo author Bryan Webber; a series of **hands-on tutorials** using all of the MCnet event generators for LHC physics; and evening **discussion sessions** with Monte Carlo authors. In addition, the school will feature lectures on:

- **Simulation of BSM Physics**
(Olivier Mattelaer and Celine Degrande (IPPP, Durham))
- **Matching and Merging**
(Stefan Prestel (DESY, Hamburg))
- **Simulation of High Energy QCD**
(Jeppe Andersen (IPPP, Durham))
- **Boosted Particle Techniques**
(Simone Marzani (IPPP, Durham))
- **Boosted Particle Searches**
(Jon Butterworth (UCL))
- **Statistics for Limit Setting**
(Andreas Weiler (CERN))
- **Industrial Application: Monte Carlo Modelling of Nuclear Reactions**
(Albrecht Kyrieleis (AMEC Clean Energy, Warrington))

A registration fee of £100 covers the participant's portion of accommodation and meals for the duration of the school - all other local expenses are funded by the EU through the MCnetITN Initial Training Network. Fee waivers and travel bursaries are available for participants from Convergence Regions of the EU and others in financial need, whose participation would be otherwise be prevented.

Applications must be received by 10 June 2014. Bursary decisions will be made after that date - students that have an urgent need for earlier information on acceptance should contact the organisers.

The Lake District is the UK's most visited National Park and offers a wide variety of outdoor activities. The school will include one afternoon of activities, but students wanting to spend longer in the Lake District may take advantage of accommodation at a discounted rate before or after the school. Details will be given on acceptance to the school.

Further details including application procedures can be found at the school homepage.

***

Göttingen 2013
--------------

The Monte Carlo School in 2013 will be organised by the Göttingen team. It will take place at Maria Spring, August 5th to August 9th. The school provides a five day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures and practical sessions. The school is aimed at advanced doctoral students and young postdocs.
The school will be based around a core lecture series on event generator physics to be given by Peter Richardson. There will be additional lectures on statistics, higher-order QCD calculations and the merging of matrix-elements and parton showers. We will have a special lecture by Manuel Bähr from Blue Yonder on industry applications. Furthermore there will be a poster session presenting the work of participants.

Further details including application procedures can be found at the school homepage.

***

Karlsruhe 2010
--------------

The Monte Carlo School in 2010 will again be a joint school with CTEQ. The school will be held in Lauterbad (Black Forest) between July 26th and August 4th. More information can be obtained from the school homepage.

***

Lund 2009
---------

The Monte Carlo School in 2009 will be held in Lund between the 1st and 4th of July. The school will provide a four day course of training in the physics and techniques used in modern Monte Carlo event generators via a series of lectures and practical sessions. The school is aimed at advanced doctoral students and young postdocs.
The school will be based around a core lecture series on event generator physics to be given by Frank Krauss. The theme of the rest of the programme will be heavy quarks and jets. There will be lectures on the general theory of heavy quark production by Eric Laenen, and special lectures on the top quark mass by Andre Hoang and jet definitions by Matteo Cacciari. There will also be lectures by Paolo Nason on matrix element/parton shower matching techniques. Furthermore there will be presentations by students who have been involved in the MCnet program of short-term fellowships, and there will be a special lecture by Carsten Peterson on the use of Monte Carlo techniques in Biophysics.

Further details including application procedures can be found at the school homepage.

***

CTEQ 2008
---------

The Monte Carlo School in 2008 will be organized together with the CTEQ collaboration's annual summer school in Debrecen, Hungary, the 8th-16th of August. More information can be obtained from the school homepage.

***

Durham 2007
-----------

The first MCnet Monte Carlo School was held in Durham the 18th to 20th of April 2007. Please see the school homepage for more details.