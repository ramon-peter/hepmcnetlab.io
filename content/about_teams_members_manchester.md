---
title: "About"
lead: "Teams > Manchester members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Core team
---------

***

##### Mike Seymour (Team Leader)

![](/img/manchester/manchester_member_seymour.jpg) 

Mike Seymour is Professor of Particle Physics and Chair of Theoretical Physics in the School of Physics and Astronomy of the University of Manchester. He is the MCnet Network Co-ordinator, Project Manager and Manchester Team Leader. He is also an author of Herwig.

##### Jeff Forshaw

> [Personal page](https://www.research.manchester.ac.uk/portal/en/researchers/jeffrey-forshaw(2d30102e-36e1-4561-9001-2094baaf591d).html)

##### Andrzej Siodmok

> [Personal page](http://siodmok.web.cern.ch/siodmok/)

##### Matthew de Angelis

Matthew is a PhD student at Manchester working on CVolver, an amplitude level parton shower.

##### Kiran Ostrolenk

Kiran Ostrolenk is a PhD student working on NLO Matching within Herwig.

Kiran has also currently implemented helicity recycling in MadGraph. This means making sure that an external spinor or internal propagator is only calculated once at a given helicity. This is important when summing an amplitude over all possible helicities. This has resulted in a 50% speed increase for complex processes such as *gg->ttbargg* and *pp->W+W-jj*.

##### Baptiste Cabouat

Baptiste Cabouat is a PhD student who works on [dShower](https://www.hep.manchester.ac.uk/u/kirano/dShower/), a double PDF parton shower.

##### Jack Holguin

Jack is a PhD student working on CVolver, an amplitude level parton shower.

Other MCnet-related staff
-------------------------

***

##### Brian Cox

> [Personal page](http://example.com/)

##### Mrinal Dasgupta

> [Personal page](https://www.research.manchester.ac.uk/portal/en/researchers/mrinal-dasgupta(a209b1a4-0a86-4d9d-bab5-753fdb4d7180).html)