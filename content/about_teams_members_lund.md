---
title: "About"
lead: "Teams > Lund members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Current members
---------

***

##### Leif Lönnblad (Team Leader)

![](/img/lund/lund_member_lonnblad.jpg)

Leif Lönnblad is the team leader for the Lund group and is the author of ThePEG
and the Ariadne program. He is also involved with the Rivet program developed
within the Cedar project. His main research interests are multi-particle
production and the modelling of perturbative QCD cascades, in particular for
small-x processes.

##### Christian Bierlich

![](/img/lund/lund_member_bierlich.png)

Christian Bierlich is a postdoc mainly working on heavy-ion applications of the
DIPSY program in the Ariadne project.

##### Smita Chakraborty

![](/img/lund/lund_member_chakraborty.png)

Smita Chakraborty is a MCnetITN3 PhD student working on the development of
hadronisation within the Ariadne project.

In particular, the first project was to develop a string "shoving" model to
describe collective effects in heavy-ion collisions in terms of string
interactions within the Lund model. This is done by constructing the Lorentz
transformation to a symmetric frame where two string pieces are in parallel
planes to calculate the resulting pairwise force due to the space-time
overlap. Considering all such pairs of string pieces in a collision, the
resulting total momentum change on a string piece is calculated. There is
ongoing work to implement this along with the rope hadronization model in
PYTHIA8.

The model will be further expanded for specific processes in heavy ion
collisions, for example for heavy quarkonia production processes where the
string geometry changes due to the heavy mass of the quarks. Another prospective
project is to study the effects of string interactions on jets, known as jet
quenching and to analyze the production yields of different hadron species in
leading jets.

##### Leif Gellersen

![](/img/lund/lund_member_gellersen.png)

Leif Gellersen is a MCnetITN3 PhD student involved with the Pythia project. His
project involves the improvement of parton showers through correction with
additional matrix elements. A first project was related to tuning of Monte Carlo
event generators. Another project is the implementation of scale and scheme
variations in UNLOPS merging, which are now implemented in Pythia 8. Further
work is directed towards the implementation of matching and merging to higher
orders and higher precision, which is anticipated to be of high relevance for
future precision measurements, especially at electron positron colliders. Last
but not least, he works on fixed colour parton showers with matrix element
corrections within the DIRE parton shower, which will be of interest for
studying interference effects between QCD, QED and dark photon signatures, and
can be used in the context of event deconstruction.

##### Torbjörn Lundberg

![](/img/lund/lund_member_lundberg.png)

Torbjörn Lundberg is a PhD student working on the development of hadronisation
within the Plugin project. In particular he is developing a colour reconnection
model to describe collective effects in heavy-ion collisions in terms of string
and dipole interactions within the Lund model and implement the this in PYTHIA8.

##### Stefan Prestel

![](/img/lund/lund_member_prestel.png)

Stefan Prestel is a lecturer mainly working on matching matrix elements with
parton showers.

##### Harsh Shah

![](/img/lund/lund_member_shah.png)

Harsh Shah is a PhD student mainly working on heavy-ion applications of the
DIPSY and Pythia8 programs in the Ariadne project.

##### Malin Sjödahl

![](/img/lund/lund_member_sjodahl.png)

Malin Sjödahl is an assistant professor in Lund and works with improving parton
showers with sub-leading Nc contributions.

##### Torbjörn Sjöstrand

![](/img/lund/lund_member_sjostrand.png)

Torbjörn Sjöstrand is the main author of the Pythia program. His research covers
almost all aspects of multiparticle production, mainly at e+e-, ep and pp
colliders. He is a member of the MCnet advisory committee.

##### Marius Utheim

Marius Utheim is a MCnetITN3 PhD student working in the Pythia project with
hadron rescattering. In the current implementation of Pythia, after hadrons have
been produced in the hadronization stage in Pythia and Bose-Einstein corrections
have been made, the outgoing hadrons no longer interact with each other. This is
despite the fact that realistically one would expect hadronic rescattering,
i.e. hadrons colliding and interacting with each other in processes like elastic
and resonant scattering. These interactions have never been implemented because
they have been thought to have little effect on the observable
distributions. However, more recently it has been suggested that they can
havenon-negligible contribution in high multiplicity events after all. One
effect that might be observed is increased pT for heavier hadrons, similar to
the collective behaviour seen in quark-gluon plasma.

This project is about implementing of hadronic rescattering in Pythia and
studying how it affects the event. One particularly central part of this
simulation is calculating cross-sections for various low energy (<10 GeV)
processes. Once this has been implemented, the underlying mechanisms can also be
used to make Pythia more accurate when simulating collisions where the original
beam particles have low energy.

##### Andrew Lifson

##### Robin Törnkvist

##### Korinna Zapp



Former members
--------------

***

##### Johannes Bellm

![](/img/lund/lund_member_bellm.png)

Johannes Bellm is a Postdoc since 10/2017. His research topic is the matching and merging of higher order calculations at fixed order with the parton shower in Herwig.

##### Christine Rasmussen

Christine Rasmussen is a PhD student working on the Pythia project.

##### Christopher Plumberg

##### Christian Reuschle

Former short-term students
--------------------------

***

##### Armando Bermudez Martinez

Azimuthal di-jet correlations: Recently in CMS, we have measured the azimuthal separation between the two leading jets in multi-jet events when these are approximately back-to-back. We have also measured the more exclusive and complementary azimuthal separation between the leading jets when the presence of at least an extra much softer jet is required in the event. We have observed a tension between the data and the theory predictions from parton shower event generators as well as from improved predictions using higher order matrix elements.

Part of the effort within the three month MCnet project was to investigate, from the parton shower phenomenology point of you, the azimuthal separation between the two leading jets when these two are approximately back-to-back. We studied the role of the renormalization scale choice in the evolution of the parton shower. In addition, investigated the dependence on the handling of the soft gluon coherence in the shower by studying the pt ordered dipole shower from PYTHIA8, the matrix element corrected shower provided by the VINCIA generator, the angular order shower from HERWIG7, and also the DIRE shower which allows the use of next-to-leading splitting kernels in the evolution. Using PYTHIA8 we assessed the influence of the recoil strategy as well as the cut-off scale for the shower.

We investigated the possibility of measuring the azimuthal separation between the leading jets also in bins of rapidity. This could allowed the enhancement of specific QCD channels and the possible tagging of quark/gluon-like jets in back-to-back topologies. In addition we studied other observables that can be measured in a correlated way with the azimuthal angular separation between the leading jets. In this direction we investigated the jet structure in back-to-back topologies employing for this the so called jet angularities.

As part of the project we also investigated the disagreement observed by CMS for the minimum azimuthal separation in 4-jet events when merged LO samples with up to four partons in the final state are compared to data.

The investigations and subsequent results have been included as a very important complementary part of my PhD thesis.

##### James Black

![](/img/lund/lund_member_black.jpg)

James is a PhD student working on the High Energy Jets project.

##### Naomi Cooke

Quarkonium production in parton showers: Quarkonia production has been a long-standing puzzle in particle
physics. These are mesons which contain quarks of the same flavour,
for example J/ψ. The polarisation measurement of J/ψ is expected to
have significant transverse polarisation at large pT, but has been
experimentally observed to be consistent with zero. Hard production of
onia processes using NRQCD formalism are available in the Pythia8
framework. However, these processes alone cannot fully describe the
data; LHCb and CMS have shown with normalised cross section
measurements of z = pT (J/ψ)/pT (jet) that J/ψ’s are produced softer
than expected. Hence the need to incorporate onia production within the
parton shower.

Therefore, the aim of this project is to incorporate the main splitting
functions of various quarkonia into Pythia8. Then compare results with
Ariadne and LHCb/CMS data as cross checks.

##### Oleh Fedkevych

Four-jet DPS production in pp and pA collisions in Pythia8: In collaboration with J. Bellm, J. Gaunt, A. Kulesza, L. Lönnblad and T. Sjöstrand. In spite of the recent progress in both theoretical and experimental studies many aspects of multiple parton interactions (MPI) still require a detail investigation. In particular, double parton scattering (DPS) processes can play a dominant role for some specific kinematic regions of multi-jet production, especially in proton-nucleus (pA) collisions where a total DPS cross section is approximately 3A times bigger as a corresponding total DPS cross section in proton-proton (pp) collisions. It is also known that DPS is sensitive to parton correlations of various types and that a combined study of DPS in pp and pA collisions at the LHC will provide us a deep insight into a protonâ€™s structure and non-perturbative dynamics of its constituents. However, a complexity of the problem leads to different models of DPS currently available in the literature. In particular there are several approaches to model so called double parton distribution functions (dPDFs) which are important ingredients for every Monte Carlo DPS simulation since dPDFs, among other effects, account for longitudinal parton correlations, flavour and momentum conservation and also for so called "1v2" splitting contribution, a situation when one initial state parton splits perturbatively into two partons which then take part in two hard interactions.

The goal of the project is to compare predictions of a model of DPS built into the Pythia event generator against predictions of other models of DPS available on the market. In particular we have studied two dif- ferent ways to model dPDFs, namely double DGLAP evolution equations and the approach of Pythia based upon dynamical modification of standard collinear PDFs. We have found that the dPDFs modelled within both frameworks obey the same set of generalized sum rules and that both approaches have rather a similar treatment of the "1v2" splitting contribution caused by a gluon splitting into quark antiquark pair at low values of Bjorken-x. We also have found that both approaches have quiet strong disagreement in description of "1v2" contributions at high values of Bjorken-x. Additionally we have studied the impact of both models of dPDFs on various DPS-sensitive differential distributions and established different regions of a phase space where existing differences may become relevant for Monte Carlo predictions.

The model of DPS being used in Pythia has inherited its main concepts from the original Pythiaâ€™s model of MPI. Which, in particular, implies that only PDFs used to generate a second hard interaction are modified in order to account for momentum and flavour conservation. This procedure, however, introduces asymmetry of the Pythiaâ€™s dPDFs and is in contradiction with the double DGLAP approach. This discrepancy was removed and, starting from Pythia version 8.240, DPS events are generated using symmetrized dPDFs.

It is know that the additional enhancement ∼ 3A of a total DPS cross section in pA collisions is due to DPS processes involving one incoming proton and two different nucleons, so called DPS II contribution. Recently a new model of pA collisions called Angantyr was implemented into the Pythia which, among others, allows to simulate some DPS processes in pA collisions. The approach being used in Angantyr differs in some aspects from the approach being used in currently available publications on DPS. In particular, Angantyr models DPS II contribution via pomeron exchange and, additionally, possesses a non-trivial collective behaviour which leads to contributions similar to those from nuclear shadowing effect. We have compared the predictions of the Angantyr model against theoretical computations currently available in the literature and found that it demonstrates a correct dependence of a total DPS cross section on a total number of nucleons. In addition to it we have studied how the enhancement of a total DPS cross section depends on different jet cuts and found a way to get an additional enhancement of a total DPS cross section in pA collisions comparing to a total DPS cross section in pp collisions evaluated for the same set of cuts.

In October 2018 some of the aforementioned results were presented at the MPI@LHC Workshop in Perugia, Italy.

##### Mees van Kampen

TMD shower in Pythia: The Pythia8 event generator implements many aspects of a detailed calculation of high energetic hadronic collisions. It is mainly focussed on simulating proton-proton collision processes as in the LHC, however, it is possible to generate events with other beam types such as electron beams for DIS events. Using “vanilla-”Pythia, one has many hard scattering calculation possibilities, the standard parton showers for initial- (ISR) and final- (FSR) state radiation, multi-parton interactions and hadronization models to its dispose. The parton showers simulate the emissions that a proton undergoes before and after the hard process. The showers and MPI are interleaved, which means that they are performed simultaneously when evolving backwards in the transverse momentum scale.

With recently new developed ideas to do event generation using transverse momentum dependent parton densities (TMDs), the aim is to understand more about the role of the current ISR and FSR before starting to implement a new shower in the Pythia framework. Subject of this study are the event kinematics and generated pT scales of emissions. Checks with a single parton emission in DIS have been performed in Pythia to reveal some information about the first parton splitting and the event kinematics. Scatter plots of the first emission in DIS gave a hint that the method of dealing with recoil effects influence the distribution of parton emissions in the phase space. Larger transverse momenta are reached when applying a global recoil instead of a recoil only within a dipole. With checks that involve multiple splittings in one event and looking in different frames of reference, we observed that from the DIS studies we cannot draw strong conclusions on the recoil prescriptions. However, with more skills and knowledge on the procedures from Pythia, a starting point is made to develop a modified version of the shower to include TMDs. The SimpleSpace shower will be used as a template for the TMD shower. Some of the elements can be adapted according to the PB method. The evolution scale will not anymore be related to the transverse momentum, but to the rescaled transverse momentum. The picking of possible scales - done with the veto algorithm - needs to be done according to a Sudakov that is calculated using TMDs. Eventually, the TMD initial shower needs to be interfaced with the final state shower appropriately.

##### Adam Takacs

Spacetime structure of parton showers with different orderings with and without medium: In the project description I offered two studies after reconciling with possible
collaborators from Lund University:

- To study the modeling uncertainties emerging from different choices of ordering variables in parton showers in event generators.
- To study the possible modification of hadronization due to the quark-gluon plasma in heavy-ion collisions.

Unfortunately, I arrived in Lund when the virus was peaking, which made the planned
collaboration difficult. Moreover, my collaborators at Lund were on their paternity
leave, making the situation more challenging. Therefore, as it was agreed on, I worked
on two new projects:

- To do a higher-order, analytic calculation of the jet-substructure observable dynamical grooming. This calculation improved previous precision up to next-to-next-to-double-logarithmic accuracy and made it possible to compare with recent experimental measurements.
- To calculate several jet observables with jet quenching, by using coherent energy loss combined with the so-called Gyulassy-Levai-Vitev medium induced emission spectrum. In this project, several novel heavy-ion techniques were tried, such as Quantile technique, Topics Modeling, di-jet production comparison with gamma-jet.

Both of these projects ended in publications by the end of my stay and so in several
conference talks, those are listed at the end of the report. In addition to these, I gave
a seminar in the institute and when my collaborators returned, we started to work on
the initially proposed projects, however, results have not been ready for publication.

Papers:

Paul Caucal, Alba Soto-Ontoso and Adam Takacs, Dynamical grooming meets LHC data: 10.1007/JHEP07(2021)020
Adam Takacs and Konrad Tywoniuk, Quenching effects in the cumulative jet spectrum: 10.1007/JHEP10(2021)038

##### Spyridon Argyropoulos

##### Helen Brooks

##### Nadine Fischer

##### Marek Sirendi

##### Radek Zlebcik

Associate members of the Lund team at Fermilab
----------------------------------------------

***

##### Stephen Mrenna

Associate members of the Lund team in Monash
--------------------------------------------

***

##### Christian Preuss

##### Peter Skands

##### Rob Verheyen