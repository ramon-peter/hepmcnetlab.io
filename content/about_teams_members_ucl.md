---
title: "About"
lead: "Teams > UCL members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Current members
---------------

***

##### Jon Butterworth (Team Leader)

![](/img/ucl/ucl_member_butterworth.png) 

Professor of Physics at UCL, working on the ATLAS experiment. Supervisor of UCL ESR (Joanna Huang) and co-coorindator for UCL node. 2019 MCnet school organising committee. MCnet projects: CEDAR (Rivet and Contur)

[More info](https://iris.ucl.ac.uk/iris/browse/profile?upi=JMBUT84), [personal webpage](https://www.hep.ucl.ac.uk/~jmb/), [blog](https://lifeandphysics.com/).

##### Christian Gütschow

STFC PDRA working on [ATLAS](https://atlas.cern/) experiment. MCnet projects: CEDAR ([Rivet](https://rivet.hepforge.org/)). [More information](http://cgutscho.web.cern.ch/cgutscho/).

##### Aidan Kelly

STFC PhD student working on ATLAS experiment. MCnet projects: CEDAR (Generator, Contur user, Rivet contributor)

##### Peng Wang

PhD student at UCL, working on [ATLAS](https://atlas.cern/) experiment. MCnet projects: CEDAR ([Contur](https://hepcedar.gitlab.io/contur-webpage/))

##### Ben Waugh

Senior research fellow. MCnet projects: Contur. [More information](https://iris.ucl.ac.uk/iris/browse/profile?upi=BMWAU34)

##### Yoran Yeh

PhD student at UCL, working on [ATLAS](https://atlas.cern/) experiment. MCnet projects: CEDAR ([Contur](https://hepcedar.gitlab.io/contur-webpage/), Yoda)

##### Luzhan (Tony) Yue

PhD student at UCL, working on [ATLAS](https://atlas.cern/) experiment. MCnet projects: CEDAR ([Contur](https://hepcedar.gitlab.io/contur-webpage/))

