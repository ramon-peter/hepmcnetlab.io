---
title: "About"
lead: "Committees"
menu:
  side_about:
    name: "Committees"
    weight: 11
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Collaboration Board
------------------------

***

MCnet is governed by the Collaboration Board, a group of senior MCnetters:

- Mike Seymour
- Jeppe Andersen
- Andy Buckley
- Jon Butterworth
- Steffen Schumann
- Stefan Gieseke
- Christian Gütschow
- Fabio Maltoni
- Leif Lonnblad
- Frank Krauss
- Torbjorn Sjostrand
- Malin Sjödahl
- Peter Richardson
- Claude Duhr
- Peter Skands
- Tilman Plehn
- Andrzej Siodmok
- Simon Plätzer
- Stefan Prestel
- Olivier Mattelaer
- Celine Degrande
- Marek Schönherr
- Frank Siegert
- Stefan Hoeche

The Student and Postdoc Committee
---------------------------------

***

The S&P committee help to organise our network meetings and also represent student and postdoc views at our monthly management committee meetings. You can [contact them by email](mailto:studentcommittee@montecarlonet.org). Current members are:

- Smita Chakraborty
- Luca Mantani
- Patrick Kirchgaesser
- Andrew Lifson
- Simon Luca Villani

Advisors
--------

***

- Witold Pokorski
- Monica D'Onofrio
- Filip Moortgat
- Andreas Morsch
- Gloria Corti