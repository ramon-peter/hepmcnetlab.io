---
title: "About"
lead: "Teams"
menu:
  side_about:
    name: "Teams"
    weight: 8
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
---

The work of MCnet is divided into teams, following the original ITN funding
structures. Although each is named after the original coordinating institution,
several include members of other academic institutes:

***


Manchester
----------

![](/img/manchester/manchester.jpg)

The Manchester team works on the Herwig project. Most of its members are in the
Manchester particle-physics group, which encompasses both theory and experiment;
several of the team members are on the [Atlas experiment](http://atlas.ch/).

The team also includes Bryan Webber of the [Cambridge high energy physics
group](https://www.hep.phy.cam.ac.uk/), also working on Herwig, Andrzej Siodmok
in Cracow and members of the CERN Partner.

The Manchester group is part of the [Lancaster-Manchester-Sheffield Consortium
for Fundamental Physics](https://www.lancaster.ac.uk/physics/research/) and
includes Jeff Forshaw and Brian Cox, authors of the POMWIG Monte Carlo, who are
planning to contribute to a new model of diffraction in Herwig, and Mrinal
Dasgupta, expert in QCD resummation.

> [View team members]({{< ref "/about_teams_members_manchester" >}})

***


Durham
------

![](/img/durham/durham.png)

The Durham team has members from Durham and Edinburgh Universities. It is part
of the Institute for Particle Physics Phenomenology (IPPP) which is a
world-leading phenomenology group with approximately 16 academic staff, 19
postdocs and 38 postgraduate students. There is an advanced series of graduate
lectures covering the full range of modern phenomenology, a regular series of
seminars together with a programme to bring senior academics for longer
periods. This provides an excellent training environment for the students,
particularly in Monte Carlo simulations which encompasses many different areas
of research, with the students having both lectures from and easy access to
world leading experts in most areas of phenomenological research.

In addition the university runs many courses in transferable skills, as part of
the PhD programme, which will be available to the students. The IPPP grant from
STFC provides excellent computing and secretarial support and the existing
facilities for visitors mean both visiting members of the network and the ESR
residencies can be accommodated.  The IPPP hosts one of the largest
high-throughput computing clusters at any UK university, and coordinates the
efforts of the PhenoGrid, giving users access to further grid resources.

> [View team members]({{< ref "/about_teams_members_durham" >}})

***


Göttingen
----------

![](/img/goettingen/goettingen.svg)

The Göttingen team has members from the Universities of Göttingen, Dresden, and
Heidelberg, and is involved in the development of the Sherpa generator. The
group leader is Steffen Schumann from the Institute for Theoretical Physics
University of Göttingen. The Göttingen team keeps strong ties to the other
Sherpa developers, in particular at the University of Durham, the University of
Dresden and FNAL (Fermilab). Our research interests range from QCD parton
showers, fixed-order calculations in QCD and the electroweak theory to
(semi-)analytic resummation and physics beyond the Standard Model.

> [View team members]({{< ref "/about_teams_members_goettingen" >}})

***


Glasgow
-------

![](/img/glasgow/glasgow.png)

The University of Glasgow team involves members of the CEDAR/Rivet project, and
has strong links to related activity in Durham and UCL.

Short-term studentships at Glasgow focus on interpretations of MC physics, from
use of precision features like MC systematic errors to generator tuning and BSM
limit-setting using MC generators and analysis tools such as Rivet. Here are
some examples of project possibilities at Glasgow:

- development of the Rivet toolkit for handling of theory systematic uncertainties and heavy-ion physics;
- reinterpretation of LHC BSM search analyses using Rivet and fast parametrisations of detector biases and efficiencies;
- use of Rivet's large collection of measurement analyses to make BSM interpretations via the TopFitter and Contur projects;
- optimisation and extension of parton density calculations via the LHAPDF framework (e.g. adding support for resolved-photon PDFs);
- statistics and feature development in the YODA and Professor libraries.

> [View team members]({{< ref "/about_teams_members_glasgow" >}})

***


Karlsruhe
---------

![](/img/karlsruhe/karlsruhe.png)

The Karlsruhe team has members from the Karlsruhe Institute for Technology (KIT)
and members of the Vienna Partner.

The research in the Karlsruhe team is centered around the event generator
Herwig++. The main topics of research are the development of matching and
merging strategies of parton showers and higher order perturbative calculations
and the development of a model for Underlying Event and Minimum Bias physics.

The environment in Karlsruhe is ideal for training ESRs. There are two theory
institutes, the Institut für Theoretische Physik and the Institut für
Theoretische Teilchenphysik with a large number of academic staff and postdocs
at the university with expertise in Higgs-physics, B-physics, Multiloop
computations and other LHC/Monte Carlo related areas. This is complemented with
a large involvement in present and future collider experiments (CMS, Belle) and
neutrino experiments and astroparticle physics at the Institut für
Experimentelle Kernphysik at the North Campus. The regular curriculum includes
advanced courses in theoretical and experimental particle physics. In depth
teaching for all graduate students is supplemented by the Graduiertenkolleg
[Elementary particle physics at highest energy and
precision](https://www.kceta.kit.edu/grk1694/english/index.php) and the graduate
school KSETA.

> [View team members]({{< ref "/about_teams_members_karlsruhe" >}})

***


Louvain
-------

![](/img/louvain/louvain.jpg)

The Louvain team is part of the Centre for Cosmology, Particle Physics, and
Phenomenology (CP3), founded in 2003 at the Université Catholique de Louvain
(UCL). CP3 hosts research on particle detectors, high energy particle physics,
phenomenology, theory of the fundamental interactions and cosmology, equally
strongly on the experimental and theoretical fronts. The aim of the Centre is to
bring together researchers in these scientific fields and to encourage
collaboration. The CP3 group consists of eleven staff members (seven
experimentalists and four theorists), more than fifteen postdocs, twenty-eight
Ph.D. students and a variety of master and bachelor students. All scientific
activities, such as weekly seminars or conference organization at the national
and international level, are always run in conjunction between experimental and
theory group.

A very important and quite unique branch of activities is the development of
tools for phenomenology, from a package
([FeynRules](http://feynrules.irmp.ucl.ac.be/)) to implement the Lagrangians of
new physics models, to matrix element-based generators (MadGraph5_aMC@NLO) and
derived packages (MadDM,MadDump,MadSpin) to universal detector simulation
packages ([DELPHES](https://cp3.irmp.ucl.ac.be/projects/delphes)). The Monte
Carlo group is involved in collaborations with MCNet, with several other centres
in Europe (e.g., CERN, Paris, NIKHEF, Bologna) in the US (e.g., Fermilab, UIUC)
and Asia (IHEP).

CP3 has well-established outlets for academic training at all levels. A series
of weekly seminars are organized, in which scientists from all over the world
are invited to share their expertise on a specific subject. In parallel, the
group holds internal lunch seminar meetings, where the most recent results in
theory and experiment, achieved by group members, are informally discussed. All
these activities are extremely well attended and give an excellent opportunity
for researchers, from students to staff members, to meet colleagues, discuss,
and develop new ideas.

> [View team members]({{< ref "/about_teams_members_louvain" >}})

***


Lund
----

![](/img/lund/lund.png)

The Lund team has members from Lund University and members of Monash University.
The team is very experienced in modeling multi-particle production processes and
implementing these models in event generators. Besides the team members, the HEP
group at the Theoretical Physics department also includes Gösta Gustafson who is
well known for his work on the models implemented in the Lund-family of event
generators, Johan Bijnens who works in the field of chiral perturbation theory,
and four PhD students.

The HEP group has close links to the Complex Systems group at the same
department, where most of the staff are former HEP researchers. This group is
active in the fields of bio-informatics, modeling of protein structure, and
machine learning.

The team also collaborates closely with the experimental HEP group at the
Department of Physics in Lund. This group is active at the H1, Phenix, ATLAS and
Alice experiments.

> [View team members]({{< ref "/about_teams_members_lund" >}})

***


UCL
---

![](/img/ucl/ucl.png)

The core UCL team includes experts on Monte Carlo event generator development,
tuning and usage, as well as experimentalists mostly working on ATLAS, but with
experience from HERA, Tevatron and elsewhere, making relevant measurements at
collider experiments. We contribute mainly to the CEDAR MCnet project,
especially to the development and maintainance of Rivet and Contur, and to the
storage and reinterpretation of data via HepData and other tools. We work
closely with local theorists with expertise in BSM model bulding, precision
Standard Model calculations and (Beyond-the-Standard)-Model building.

More information on the wider UCL HEP group can be found [here](https://www.hep.ucl.ac.uk/).

> [View team members]({{< ref "/about_teams_members_ucl" >}})

***


Vienna
---

![](/img/vienna/vienna.png)

The Vienna Team is led by Simon Plätzer and is embedded in the Particle Physics
Group at the University of Vienna, which hosts expertise in several
complementary fields centered around phenomenology of the Standard Model of
particle physics.

The Particle Physics Group has a strong focus on QCD and Effective Field Theory
methods, and the Vienna team contributes to the development of the Herwig event
generator in the areas of matching, parton shower development, colour
reconnection and hadronization, with close links to the Karlsruhe team. We also
address the theoretical foundations of parton showers in an effort to improve on
their accuracy, and are involved in developing connections between analytic
predictions and parton shower algorithms to identify the precise definition of
fundamental parameters such as quark masses. Within a closely related effort and
in collaboration with Jeff Forshaw of the Manchester team, we develop the
CVolver framework for a parton shower evolution at the amplitude level, both
concerning its theoretical basis and innovative Monte Carlo methods and their
implementation.

> [View team members]({{< ref "/about_teams_members_vienna" >}})

***


CERN
---

Members from both TH (theory) and SFT (physics software) groups of the physics
department of CERN. CERN hosts one network meeting per year.

***


Heidelberg
---

Led by Tilman Plehn.

***


Monash
---

Australian team in Melbourne, led by Peter Skands. Monash hosted the 2018 school at their European Campus in Prato, Tuscany.

***


Cracow
---

In Cracow, Poland, with the members of Institute of Nuclear Physics PAN, led by Andrzej Siodmok.

***


Dresden
---

With the members of Technische Universität Dresden, led by Frank Siegert.

***


Edinburgh
---

Led by Jenni Smillie of the University of Edinburgh, working on HEJ and associated QCD radiative-corrections physics.

***


Fermilab
---

Fermi National Accelerator Laboratory, USA, with the members of the theory division.
