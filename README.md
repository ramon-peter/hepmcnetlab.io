![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

MCnet website using GitLab Pages and [Hugo].

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.


## Building locally

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/`.

You can make changes and run `hugo` to rebuild. If you need to add new pages,
put them in `content/`; if you need to add images or other resources, put them
in `static/img/`. For any bigger reworkings, contact the maintainers (repo admins).

When happy with the changes, commit them (including any added files), push, and
make a merge request to the main branch for updating the live website.


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
